龙芯测试机

- https://lx.atzlinux.com:24359/atzlinux-cd/

## IP
- 机器内网 IP： 10.40.64.22
- 机器外网 IP： 114.242.206.180

## 端口映射

- 24359 --> 443
- 11032 --> 22

## 硬件信息
- https://linux-hardware.org/?probe=014bdabb68
